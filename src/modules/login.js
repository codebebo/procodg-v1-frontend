import React, { Component } from 'react';
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { 
	Container, 
	Row, 
	Col,
	Card,
	CardBody,
	Button,
	Form, FormGroup, Input, Label
	
} from 'reactstrap';
import {authLogin} from '../store/actions/authAction';

class Login extends Component {

    constructor(props) {
    	super(props);
        this.handleReset = this.handleReset.bind(this);
    	this.handleChange = this.handleChange.bind(this);
    	this.handleLoginSubmit = this.handleLoginSubmit.bind(this);
	    
	    this.state = {
	      	errors: {}, 
	      	touched: {}
    	};
  	}

  	componentDidMount(){
  	    	
  	}

    componentWillReceiveProps(nextProps){
        if(nextProps.auth.data) {
            window.location.reload();
            //sessionStorage.setItem("userToken", nextProps.auth.data.token);
        }
        if(nextProps.auth.error) {
            this.setState({
                errors: { ...this.errors, username: nextProps.auth.error.email }
            });
        }      
    }

    handleReset = () => {
        this.refs.form.reset();
    }

  	handleChange = event => {
        const { name, value } = event.target;
        this.setState(
            {
                [name]: value,
                touched: { ...this.state.touched, [name]: true }
            },
            () => {
                this.validate();
            }
        );
    };

    validate = () => {
        const { username, password } = this.state;

        if (!username) {
            this.setState({
            	errors: { ...this.errors, username: "Please enter email" }
            });
            return false;
        } else if (!password) {
            this.setState({
            	errors: { ...this.errors, password: "Please enter password" }
            });
            return false;
        } else {
            this.setState({
            	errors: { ...this.errors, username: "", password: "" }
            });
        }
        return true;
    }

    handleLoginSubmit = event => {
        event.preventDefault();
        this.setState({loginFail: ''});
        if (this.validate()) {
        	const { username, password } = this.state;
            const params = {};
            params.email = username;
            params.password = password;
            this.props.authLogin(params);            
        } else {
            this.setState({ touched: { name: true } });
        }
    }

    render() {
        
        const GotoHome = () => {
  
            return (
                    <React.Fragment>
                    {sessionStorage.getItem("userToken") && (
                        <Redirect to='/projects' />
                    )}
                    </React.Fragment>
                );
        }

        return (
       		<React.Fragment>
               {GotoHome()}
               <div className="loginWrqpper">
                    
                    <Container>
    					<Row>
    						<Col className="loginBox">
                                
                                    <h1 className="loginLogo">ProCodG</h1>
    							<Card>
    						        <CardBody>
                                        <Form ref="form" className="" autoComplete="off" onSubmit={this.handleLoginSubmit}>
    						        		<FormGroup>
    								          	<Label for="username">Email</Label>
    								          	<Input type="username" name="username" id="userName" autoComplete="off" placeholder="Email" onChange={this.handleChange} className="login-textbox"/>
    								          	{this.state.errors["username"] && (<span style={{color: "red"}} className="bounce">{this.state.errors["username"]}</span>)}
    								        </FormGroup>
    								        <FormGroup>
    								          	<Label for="password">Password</Label>
    								          	<Input type="password" name="password" id="password" autoComplete="off" placeholder="Password" onChange={this.handleChange} className="login-textbox"/>
    								          	<span style={{color: "red"}}>{this.state.errors["password"]}</span>
    								        </FormGroup>

    								        <FormGroup>
                                                {/*<Link className="link" to="#">Forgot Password</Link>{' '}*/}
    								        	<Button type="submit" color="danger" className="pull-right">Login</Button>
    								        </FormGroup>
    						        	</Form>
    						        </CardBody>
    							</Card>
    						</Col>
    					</Row>
          			</Container>
                </div>
			</React.Fragment>
		);
	}
}
const mapStateToProps = state => {
  	return {
  		auth: state.auth
  	};
};
export default connect(mapStateToProps, { authLogin })(Login);
