import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import {authLogout} from '../../store/actions/authAction';
import { 
	Collapse,
	Navbar,
	NavbarToggler,
	NavbarBrand,
	Nav,
	NavItem,
	UncontrolledDropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem
    } from 'reactstrap';
class MainLayout extends Component {
   
    constructor(props) {
    	super(props);

	    this.toggleMenu = this.toggleMenu.bind(this);
	    this.toggleAddProjectModal = this.toggleAddProjectModal.bind(this);
	    this.logout = this.logout.bind(this);
	    this.state = {
	    	preLoader: true,
	      	isOpenMenu: false,
	      	isOpenAddProjectModal: false
    	};
  	}

  	componentDidMount() {
  		this.setState({ preLoader: false });
  	}

  	toggleMenu() {
    	this.setState({
      		isOpenMenu: !this.state.isOpenMenu
    	});
  	}

  	toggleAddProjectModal() {
    	this.setState({
      		isOpenAddProjectModal: !this.state.isOpenAddProjectModal
    	});
  	}

  	logout() {
  		this.props.authLogout();
  	}

    render() {
        return (
            <React.Fragment>
            	{this.state.preLoader && (
	            	<div className="preLoader">
				        <div className="preLoaderHeader"></div>
				        <div className="preLoaderBody"></div>
				    </div>
				)}
   				<Navbar className="mb-4 nav-header" light expand="md">
		          	<NavbarBrand>ProCodG</NavbarBrand>
		          	<NavbarToggler onClick={this.toggleMenu} />
		          	<Collapse isOpen={this.state.isOpenMenu} navbar>
						<Nav className="ml-auto" navbar>
							<NavItem>
								<Link to="/" className="nav-link">
									Home
								</Link>
							</NavItem>
							<UncontrolledDropdown nav inNavbar>
								<DropdownToggle nav caret className="header-btn header-avatar js-open-header-member-menu userLogout">
									<span className="member">
										<span className="member-initials" title="Anil Dalak (anildalak1)">AD</span>
									</span>
								</DropdownToggle>
								<DropdownMenu right>
									<DropdownItem className="logoutBtn">
										<Link to='#' onClick={this.logout}>Logout</Link>
									</DropdownItem>
								</DropdownMenu>
							</UncontrolledDropdown>
						</Nav>												
		          	</Collapse>
		        </Navbar>
		        
		        {this.props.children}
		    </React.Fragment>
        );
    }
}
const mapStateToProps = state => {
  	return {
  		
  	};
};
export default connect(mapStateToProps, { authLogout })(MainLayout);
