import React, { Component } from 'react';
import { connect } from "react-redux";
import {Editor, EditorState,convertToRaw} from 'draft-js';
import MainLayout from '../layouts/main';
import CheckAuthCmp from '../../components/checkAuth';
import ContentEditable from 'react-contenteditable'
import { 
	Container, 
	Row, 
	Col,
	Button,
	Form, FormGroup, Input, Label,
	Modal, ModalHeader, ModalBody, ModalFooter
} from 'reactstrap';
import {statusList, loadMoreStatusList, createStatusReport} from '../../store/actions/statusAction';

class StatusReport extends Component {
    constructor(props) {
    	super(props);

		this.toggleAddStatusReportModal = this.toggleAddStatusReportModal.bind(this);
		this.handleChangeStatus = this.handleChangeStatus.bind(this);
		this.onChange = (editorState) => this.setState({editorState});
	    this.handleSubmitStatus = this.handleSubmitStatus.bind(this);
	    //this.handleSearchChange = this.handleSearchChange.bind(this);
        this.staDesHandleChange = this.staDesHandleChange.bind(this);
	    this.loadMore = this.loadMore.bind(this);
	    this.state = {
	      	isOpenAddStatusReportModal: false,
	      	errors: {}, 
			touched: {},
			editorState: EditorState.createEmpty(),
            statusDesHtml: ""       
		};
		
		this.setEditor = (editor) => {
            this.editor = editor;
        };
        this.focusEditor = () => {
            if (this.editor) {
                this.editor.focus();
            }
        };

        this.styles = {
            editor: {
                border: '1px solid #ccc',
                minHeight: '6em',
                padding:'10px'
            }
        };
  	}
	
	componentWillMount() {
        this.props.statusList(this.props.match.params.id);
    }

  	componentDidMount(){
        this.focusEditor();
  	}

  	loadMore = (event, nextPage) => {
        let queryStr = document.getElementById("searchStatus").value;
  		this.props.loadMoreStatusList(this.props.match.params.id, queryStr, nextPage);
  	}
  	/* SEARCH STATUS */
    /*handleSearchChange = (e) => {
    	this.props.statusList(this.props.match.params.id, e.target.value);
    }*/

  	toggleAddStatusReportModal() {
    	this.setState({
      		isOpenAddStatusReportModal: !this.state.isOpenAddStatusReportModal
    	});
  	}

    staDesHandleChange = event => {
        this.setState({statusDesHtml: event.target.value});
    };

  	handleChangeStatus = event => {
        const { name, value } = event.target;
        this.setState(
            {
                [name]: value,
                touched: { ...this.state.touched, [name]: true }
            },
            () => {
                this.validate();
            }
        );
    };

    handleEditorChangeStatus = (e) => {
		const detail = "detail";
        this.setState(
            {
                [detail]: e.target.getContent(),
                touched: { ...this.state.touched, [detail]: true }
            },
            () => {
                this.validate();
            }
        );
  	}

  	validate = () => {
        const { title, tracking_hours } = this.state;

        if (!title) {
            this.setState({
            	errors: { ...this.errors, title: "Please enter project title" }
            });
            return false;
        } else if (!tracking_hours) {
            this.setState({
            	errors: { ...this.errors, tracking_hours: "Please enter work hours" }
            });
            return false;
        } 
        return true;
    }

    handleSubmitStatus = event => {
		event.preventDefault();
		//const detail = JSON.stringify(convertToRaw(this.state.editorState.getCurrentContent()));
        const detail = this.state.statusDesHtml;
        if (this.validate()) {
        	const { title, tracking_hours } = this.state;
            const params = {};
            params.title = title;
            params.tracking_hours = tracking_hours;
            params.detail = detail;
            this.props.createStatusReport(this.props.match.params.id, params);
            this.setState({ title: "", tracking_hours: "", statusDesHtml: "" });
            this.setState({
	      		isOpenAddStatusReportModal: !this.state.isOpenAddStatusReportModal
	    	});
        } else {
            this.setState({ touched: { name: true } });
        }
    }

    Capitalize(str){
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    render() {
        return (
       		<React.Fragment>
			   {CheckAuthCmp()}
                <div className="statusWrqpper">
           			<MainLayout>
    	       			<Container>	       				
    						<Form >
    							<Row>
    								<Col xs="12">
    							  		<FormGroup>
    							          	<Input id="searchStatus" type="text" name="search" placeholder="Search Status" onChange={this.handleSearchChange} />
    							          	<Button className="addButton" color="danger" type="button" onClick={this.toggleAddStatusReportModal}>Add Report</Button>
    							        </FormGroup>
    							    </Col>
    							</Row>        
    						</Form>
    						<Row>
    						  	<Col className="mb-4">
    							  	<ul className="timeline">
    							  		{this.props.status.list && this.props.status.list.data && (
                                    		this.props.status.list.data.map((status, index) => {
                                    			return (
    												<li key={index}>
    													{/*<div className="trackingHours">{status.tracking_hours}</div>
    													<p>{status.title}</p>
    													<p className="float-right">{status.created_date}</p>
    													<p dangerouslySetInnerHTML={{__html: status.detail}}></p>*/}

                                                        <div>
                                                            <h2 className="taskHead">
                                                                {this.Capitalize(status.title)}
                                                                <p className="float-right">Hours: {status.tracking_hours} | {status.created}</p>
                                                            </h2>
                                                            <p dangerouslySetInnerHTML={{__html: status.detail}}></p>
                                                        </div>                                                        
    												</li>
    											)
    										})
    		                            )}
    								</ul>								
    							</Col>
    						</Row>						
    						{this.props.status.list && this.props.status.list.paging.has_more && (
    							<Row><Col className="mb-12 center">
    								<Button color="primary" onClick={(event)=>this.loadMore(event, this.props.status.list.paging.next)}>Load More</Button>
    							</Col></Row>
    						)}
    	      			</Container>
    	      			
    	      			<Modal isOpen={this.state.isOpenAddStatusReportModal} toggle={this.toggleAddStatusReportModal} className={this.props.className}>
    						<ModalHeader toggle={this.toggleAddStatusReportModal}>Add Status Report</ModalHeader>
    						<Form className="modalForm" autoComplete="off" onSubmit={this.handleSubmitStatus}>
    							<ModalBody>
    						        <FormGroup>
    						          	<Label for="titleSt">Title</Label>
    						          	<Input type="text" name="title" id="titleSt" placeholder="Status Report Title" onChange={this.handleChangeStatus} />
    						          	<span style={{color: "red"}}>{this.state.errors["title"]}</span>
    						        </FormGroup>
    						        <FormGroup>
    						          	<Label for="workedSt">Worked Hours</Label>
    						          	<Input type="number" name="tracking_hours" id="workedSt" placeholder="Hours in Number" min="1" max="18" onChange={this.handleChangeStatus} />
    						          	<span style={{color: "red"}}>{this.state.errors["tracking_hours"]}</span>
    						        </FormGroup>
    						        <FormGroup>
    									<Label for="taskRepDescription">Task description</Label>
    									{/*<div style={this.styles.editor} onClick={this.focusEditor}>
                                            <Editor
                                            ref={this.setEditor}
                                            editorState={this.state.editorState}
                                            onChange={this.onChange}
                                            toolbar={{
                                            history: { inDropdown: true },
                                            inline: { inDropdown: false },
                                            list: { inDropdown: true },
                                            link: { showOpenOptionOnHover: true },
                                            textAlign: { inDropdown: true },
                                            image: { uploadCallback: this.imageUploadCallBack }
                                            }}
                                            onContentStateChange={this.editorState}
                                            placeholder="write text here..."
                                            spellCheck
                                            />                                               
                                        </div>*/}
                                        <ContentEditable
                                            className="proDetailDescription"
                                            innerRef={this.contentEditable}
                                            html={this.state.statusDesHtml}
                                            disabled={false}
                                            onChange={this.staDesHandleChange}
                                            tagName='article'
                                        /> 
    						        </FormGroup>
    				          	</ModalBody>
    				          	<ModalFooter>
    				          		<Button type="submit" color="primary">Save</Button>{' '}
    					            <Button color="secondary" onClick={this.toggleAddStatusReportModal}>Cancel</Button>
    				          	</ModalFooter>
    				        </Form>
    			        </Modal>
    				</MainLayout>
                </div>
			</React.Fragment>
		);
	}
}

const mapStateToProps = state => {
  	return {
		status: state.status
  	};
};
export default connect(mapStateToProps, { statusList, loadMoreStatusList, createStatusReport })(StatusReport);
