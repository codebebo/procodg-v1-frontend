import React, { Component } from 'react';
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import {EditorState} from 'draft-js';
import MainLayout from '../layouts/main';
import CheckAuthCmp from '../../components/checkAuth';
import ContentEditable from 'react-contenteditable'
import { 
    Container, 
    Row, 
    Col,
    Card, 
    CardText,
    CardBody,
    CardTitle, 
    CardSubtitle, 
    Button,
    Form, FormGroup, Input, Label,
    Modal, ModalHeader, ModalBody, ModalFooter
} from 'reactstrap';
import {projectList, loadMoreProjectList, createProject, deleteProject, updateProject} from '../../store/actions/projectAction';
class ProjectList extends Component {
    constructor(props) {
        super(props);
        
        this.onChange = (editorState) => this.setState({editorState});
        this.toggleMenu = this.toggleMenu.bind(this);
        this.toggleAddProjectModal = this.toggleAddProjectModal.bind(this);
        this.toggleProjectDetailModal = this.toggleProjectDetailModal.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        //this.handleEditorUpdate = this.handleEditorUpdate.bind(this);
        this.handleUpdateSubmit = this.handleUpdateSubmit.bind(this);
        this.handleSearchChange = this.handleSearchChange.bind(this);
        this.proDesHandleChange = this.proDesHandleChange.bind(this);
        this.proDesEditHandleChange = this.proDesEditHandleChange.bind(this);
        this.projectDelete = this.projectDelete.bind(this);
        
        this.state = {
              isOpenMenu: false,
              isOpenAddProjectModal: false,
              isOpenProjectDetailModal: false,
              errors: {},
              touched: {},
              editorState: EditorState.createEmpty(),
              proDesHtml: "",
              proDesId: "",
              proDesTitle: "",
              proDesEditHtml: ""
        };

        this.setEditor = (editor) => {
            this.editor = editor;
        };
        this.focusEditor = () => {
            if (this.editor) {
              this.editor.focus();
            }
        };

        this.styles = {
            editor: {
              border: '1px solid #ccc',
              minHeight: '6em',
              padding:'10px'
            }
        };
    }
    componentWillMount() {
        this.props.projectList();
    }
    componentDidMount(){        
        this.focusEditor();
    }

    componentWillReceiveProps(nextProps){
        //console.log(nextProps.projects);
    }

    loadMore = (event, nextPage) => {
        let queryStr = document.getElementById("searchProject").value;
        this.props.loadMoreProjectList(queryStr, nextPage);
    }

    /* SEARCH PROJECT */
    handleSearchChange = (e) => {
        this.props.projectList(e.target.value);
    }

    projectDelete = (event, proId) => {
        this.props.deleteProject(proId);
    }

    toggleMenu() {
        this.setState({
            isOpenMenu: !this.state.isOpenMenu
        });
    }

    toggleAddProjectModal() {
        this.setState({
            isOpenAddProjectModal: !this.state.isOpenAddProjectModal
        });
    }

    toggleProjectDetailModal(event, obj) {
        this.setState({
            isOpenProjectDetailModal: !this.state.isOpenProjectDetailModal
        });
        if(obj) {
            this.setState({
                proDesId: obj.projectID,
                proDesTitle: obj.name,
                proDesEditHtml: obj.project_detail,
            });
        }
    }

    proDesHandleChange = event => {
        this.setState({proDesHtml: event.target.value});
    };

    handleChange = event => {
        const { name, value } = event.target;        
        this.setState(
            {
                [name]: value,
                touched: { ...this.state.touched, [name]: true }
            },
            () => {
                this.validate();
            }
        );
    };    
    
    validate = () => {
        const { name, client_name, description } = this.state;
        if (!name) {
            this.setState({
                errors: { ...this.errors, name: "Please enter project name" }
            });
            return false;
        } else if (!client_name) {
            this.setState({
                errors: { ...this.errors, client_name: "Please enter client name" }
            });
            return false;
        } else if (!description) {
            this.setState({
                errors: { ...this.errors, description: "Please enter description" }
            });
            return false;
        }  else {
            this.setState({
                errors: { ...this.errors, name: "", client_name: "", description: "", project_detail: "" }
            });
        }
        return true;
    }    

    handleSubmit = event => {
        event.preventDefault();
        if (this.validate()) {
            //const project_detail = JSON.stringify( convertToRaw(this.state.editorState.getCurrentContent()) );
            const { name, client_name, description } = this.state;
            const params = {};
            params.name = name;
            params.client_name = client_name;
            params.description = description;
            params.project_detail = this.state.proDesHtml;
            this.props.createProject(params);
            this.setState({ name: "", client_name: "", description: "", project_detail: "" });
            this.setState({
                  isOpenAddProjectModal: !this.state.isOpenAddProjectModal
            });
        } else {
            this.setState({ touched: { name: true } });
        }
    }

    /* UPDATE PROJECT DETAILS */
    proDesEditHandleChange = event => {
        this.setState({proDesEditHtml: event.target.value});
    };  
    
    handleUpdateSubmit(event, prId) {
        event.preventDefault();
        const params = {};
        params.project_detail = this.state.proDesEditHtml;
        this.props.updateProject(prId, params);
        this.setState({ project_detail_update: "" });
        this.setState({isOpenProjectDetailModal: false});
    }    
    
    render() {
        return (
            <React.Fragment>
                {CheckAuthCmp()}                
                <div className="projectWrqpper">                        
                    <MainLayout>
                        <Container>
                            <Form >
                                <Row>
                                    <Col xs="12">
                                        <FormGroup>
                                            <Input id="searchProject" type="text" name="search" placeholder="Search Project" onChange={this.handleSearchChange} />
                                            <Button className="addButton" color="danger" type="button" onClick={this.toggleAddProjectModal}>Add Project</Button>
                                        </FormGroup>
                                    </Col>
                                </Row>        
                            </Form>
                            <Row>
                                {this.props.projects.list && this.props.projects.list.data && (
                                    this.props.projects.list.data.map((res, index) => {
                                        return (
                                              <Col key={index} md="4" className="mb-4" >
                                                  <Card>
                                                    <CardBody>
                                                        {res.hours && (
                                                            <CardText className="proTotalHours">Total: {res.hours} hours</CardText>
                                                        )}
                                                        <CardTitle>{res.name}</CardTitle>
                                                        <CardSubtitle>{res.client_name}</CardSubtitle>
                                                        <CardText>{res.description}</CardText>
                                                        <Button color="info" onClick={(event)=>this.toggleProjectDetailModal(event, res)} rel={res.projectID}>Details</Button>{' '}
                                                        <Link to={"/projects/"+res.projectID+"/reports"} className="btn btn-primary">Reports</Link>{' '}
                                                        {/*<Button color="danger" onClick={(event)=>this.projectDelete(event, res.projectID)}>Delete</Button>{' '}*/}
                                                    </CardBody>
                                                </Card>
                                            </Col>
                                        )
                                    })
                                )}
                            </Row>
                            {this.props.projects.list && this.props.projects.list.paging.has_more && (
                                <Row><Col className="mb-12 center">
                                    <Button color="primary" onClick={(event)=>this.loadMore(event, this.props.projects.list.paging.next)}>Load More</Button>
                                </Col></Row>
                            )}
                        </Container>
                                
                        <Modal isOpen={this.state.isOpenAddProjectModal} toggle={this.toggleAddProjectModal} className={this.props.className}>
                            <Form className="modalForm" autoComplete="off" onSubmit={this.handleSubmit}>
                                <ModalHeader toggle={this.toggleAddProjectModal}>Add Project</ModalHeader>
                                <ModalBody>
                                    <FormGroup>
                                          <Label for="exampleName">Name</Label>
                                          <Input type="name" name="name" id="exampleName" placeholder="Name" onChange={this.handleChange} />
                                          <span style={{color: "red"}}>{this.state.errors["name"]}</span>
                                    </FormGroup>
                                    <FormGroup>
                                          <Label for="exampleClientName">Client Name</Label>
                                          <Input type="client_name" name="client_name" id="exampleClientName" placeholder="Client Name" onChange={this.handleChange} />
                                          <span style={{color: "red"}}>{this.state.errors["client_name"]}</span>
                                    </FormGroup>                                                                        
                                    <FormGroup>
                                          <Label for="description">Description</Label>
                                          <Input type="textarea" name="description" id="description" onChange={this.handleChange} />
                                          <span style={{color: "red"}}>{this.state.errors["description"]}</span>
                                    </FormGroup>
                                    <FormGroup>
                                        <Label for="exampleProjectDetail">Project Detail</Label>
                                        {/*<div style={this.styles.editor} onClick={this.focusEditor}>
                                            <Editor
                                            ref={this.setEditor}
                                            editorState={this.state.editorState}
                                            onChange={this.onChange}
                                            toolbar={{
                                            history: { inDropdown: true },
                                            inline: { inDropdown: false },
                                            list: { inDropdown: true },
                                            link: { showOpenOptionOnHover: true },
                                            textAlign: { inDropdown: true },
                                            image: { uploadCallback: this.imageUploadCallBack }
                                            }}
                                            onContentStateChange={this.editorState}
                                            placeholder="write text here..."
                                            spellCheck
                                            />
                                        </div>*/}
                                        <ContentEditable
                                            className="proDetailDescription"
                                            innerRef={this.contentEditable}
                                            html={this.state.proDesHtml}
                                            disabled={false}
                                            onChange={this.proDesHandleChange}
                                            tagName='article'
                                        />
                                    </FormGroup>
                                </ModalBody>
                                
                                <ModalFooter>
                                    <Button type="submit" color="primary">Save</Button>{' '}
                                    <Button color="secondary" onClick={this.toggleAddProjectModal}>Cancel</Button>
                                </ModalFooter>
                            </Form>
                        </Modal>

                        <Modal isOpen={this.state.isOpenProjectDetailModal} toggle={this.toggleProjectDetailModal} className={this.props.className+" proDetModal"}>                            
                            <div>
                                <Form className="modalForm" autoComplete="off" onSubmit={(event)=>this.handleUpdateSubmit(event, this.state.proDesId)}>
                                    <ModalHeader toggle={this.toggleProjectDetailModal}>{this.state.proDesTitle} Details</ModalHeader>
                                    <ModalBody>
                                        <ContentEditable
                                            className="proDetailEditable"
                                            innerRef={this.contentEditable}
                                            html={this.state.proDesEditHtml}
                                            disabled={false}
                                            onChange={this.proDesEditHandleChange}
                                            tagName='article'
                                        />
                                    </ModalBody>
                                    <ModalFooter>
                                        <Button type="submit" color="primary">Update</Button>{' '}
                                        <Button color="secondary" onClick={this.toggleProjectDetailModal}>Cancel</Button>
                                    </ModalFooter>
                                </Form>
                            </div>                            
                        </Modal>
                    </MainLayout>
                </div>                
            </React.Fragment>
        );
    }
}
const mapStateToProps = state => {
    return {
        projects: state.projects
    };
};
export default connect(mapStateToProps, { projectList, deleteProject, loadMoreProjectList, createProject, updateProject })(ProjectList);
