import React, { Component } from 'react';
import { BrowserRouter as Router, Route,  Switch,Link } from "react-router-dom";
import Login from './modules/login';
import ProjectList from './modules/project/projectList';
import StatusReport from './modules/project/statusReport';
const NoMatch = ({ location }) => (
    <div className="big-message quiet">
    <h1>Page not found.</h1>
    <p>This page may be private. <Link to={"/"} className="btn btn-danger">Go Back</Link></p></div>
  )
class App extends Component {
    render() {
        return (
            <React.Fragment>
                <Router>
                    <Switch>
                        <Route exact path="/" component={Login} />
                        <Route exact path="/projects" component={ProjectList} />
                        <Route exact path="/projects/:id/reports" component={StatusReport} />
                        <Route component={NoMatch} />

                    </Switch>
                </Router>
            </React.Fragment>
        );
    }
}

export default App;
