export default function expiredToken() {
    sessionStorage.removeItem("userToken");
}