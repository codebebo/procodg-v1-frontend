import React from 'react';
import { Redirect } from "react-router-dom";
const CheckAuthCmp = () => {
  
    return (<React.Fragment>
        {!sessionStorage.getItem("userToken") && (
            <Redirect to='/' />
        )}
    </React.Fragment>);
}

export default CheckAuthCmp;


