import axios from "axios";
import * as config from "../../config/environment";
export const client = axios.create({
  baseURL: config.API_URL,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Api-Key':config.API_KEY
  },
  withCredentials: false
});