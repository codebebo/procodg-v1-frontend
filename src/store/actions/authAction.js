import * as types from '../types';
import { client } from '../middlewares/axios';
import expiredToken from '../../components/expiredToken';
export function authLogin(params) {
    const url = "auth/login";
    const request = client.post(url, params, {});
    return (dispatch) => {
        request.then(({data}) => {            
            if(data.error===false){
                sessionStorage.setItem("userToken", data.token);
            } 
            dispatch({type: types.AUTH_LOGIN, payload: data});
        })
        .catch(function (error) {
            //console.log(error);
            dispatch({type: types.AUTH_LOGIN_ERROR, payload: error.response.data})
        })
    }
}

export function authLogout() {
   /* const auth_token = sessionStorage.getItem("userToken");
    const url = "auth/logout";*/
   /* const request = client.post(url, {}, {
        headers: {
            'Authorization': 'Bearer ' + auth_token,
        }});*/
    return (dispatch) => {
        /*request.then(({data}) => {
            dispatch({type: types.AUTH_LOGOUT, payload: data})
            sessionStorage.setItem("userToken", "")
            window.location.reload(true)
        })
        .catch(function (error) {
            dispatch({type: types.AUTH_LOGIN_ERROR, payload: error.response.data})
        })*/
        dispatch({type: types.AUTH_LOGOUT, payload: ""});
        expiredToken();
    }
}