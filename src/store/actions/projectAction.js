import * as types from '../types';
import { client } from '../middlewares/axios';
//import expiredToken from '../../components/expiredToken';
const auth_token = sessionStorage.getItem("userToken");
export function createProject(params) {    
    const url = "projects/create";
    const request = client.post(url, params, {
        headers: {
            'Authorization': 'Bearer ' + auth_token,
        }
    });
    /*return (dispatch) => {
        request.then(({data}) => {
            dispatch({type: types.ADD_PROJECT, payload: data});
        })
        .catch(error => {
            dispatch({type: types.ADD_PROJECT_ERROR, payload: error.response.data});
        })
    }*/
    return dispatch => {
        return dispatch({
            type: "ADD_PROJECT",
            payload: request
        })
    }
}

export function projectList(keyword = '', page='', limit=12) {
    const url = 'projects?q='+keyword+'&page='+page+'&limit='+limit;
    const request = client.get(url, {
        headers: {
            'Authorization': 'Bearer ' + auth_token
        }
    })
    /*.then(({data}) => { 
            dispatch({type: types.PROJECT_LIST, payload: data});
        })
        .catch(error => {            
            dispatch({type: types.PROJECT_LIST_ERROR, payload: error});
        });
    }*/

    return dispatch => {
        return dispatch({
            type: "PROJECT_LIST",
            payload: request
        })
    }
}

export function loadMoreProjectList(keyword = '', page='', limit=12) {
    const url = 'projects?q='+keyword+'&page='+page+'&limit='+limit;
    const request = client.get(url,{
        headers: {
            'Authorization': 'Bearer ' + auth_token,
        }
    });
    /*return (dispatch) => {
        request.then(({data}) => {
            dispatch({type: types.LOADMORE_PROJECT_LIST, payload: data});
        })
        .catch(error => {
            
            dispatch({type: types.LOADMORE_PROJECT_LIST_ERROR, payload: error.response.data});
        })
    }*/
    return dispatch => {
        return dispatch({
            type: "LOADMORE_PROJECT_LIST",
            payload: request
        })
    }
}

/*export function projectDetail(projectId) {
    const url = 'projects/detail/'+projectId;
    const request = client.get(url, {
        headers: {
            'Authorization': 'Bearer ' + auth_token,
        }});
    return (dispatch) => {
        request.then(({data}) => {
            dispatch({type: types.PROJECT_DETAIL, payload: data});
        })
        .catch(error => {
            
            dispatch({type: types.PROJECT_DETAIL_ERROR, payload: error.response.data});
        })
    }
}*/

export function updateProject(projectId, params) {
    const url = 'projects/'+projectId+'/update';
    const request = client.patch(url, params, {
        headers: {
            'Authorization': 'Bearer ' + auth_token,
        }
    });
    /*return (dispatch) => {
        request.then(({data}) => {
            dispatch({type: types.UPDATE_PROJECT, payload: data});
        })
        .catch(error => {
            
            dispatch({type: types.UPDATE_PROJECT_ERROR, payload: error.response.data});
        })
    }*/
    return dispatch => {
        return dispatch({
            type: "UPDATE_PROJECT",
            payload: request
        })
    }
}

export function deleteProject(projectId) {
    const url = 'projects/delete/'+projectId+'/';
    const request = client.delete(url,{}, {
        headers: {
            'Authorization': 'Bearer ' + auth_token,
        }});
    return (dispatch) => {
        request.then(({data}) => {
            dispatch({type: types.DELETE_PROJECT, payload: data});
        })
        .catch(error => {
            
            dispatch({type: types.DELETE_PROJECT_ERROR, payload: error.response.data});
        })
    }
}