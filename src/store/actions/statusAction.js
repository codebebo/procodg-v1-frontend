//import * as types from '../types';
import { client } from '../middlewares/axios';
//import expiredToken from '../../components/expiredToken';
const auth_token = sessionStorage.getItem("userToken") || '';

export function statusList(projectId, keyword='', page='') {
    const url = 'projects/'+projectId+'/reports?q='+keyword+'&page='+page+'&limit=1';
    const request = client.get(url,{
        headers: {
            'Authorization': 'Bearer ' + auth_token,
        }
    });
    /*return (dispatch) => {
        request.then(({data}) => {
            dispatch({type: types.STATUS_LIST, payload: data});
        }).catch(error => {
            dispatch({type: types.STATUS_LIST_ERROR, payload:  error.response.data});
        })
    }*/
    return dispatch => {
        return dispatch({
            type: "STATUS_LIST",
            payload: request
        })
    }
}

export function loadMoreStatusList(projectId, keyword='', page='') {
    const url = 'projects/'+projectId+'/reports?q='+keyword+'&page='+page+'&limit=1';
    const request = client.get(url,{
        headers: {
            'Authorization': 'Bearer ' + auth_token,
        }
    });
    /*return (dispatch) => {
        request.then(({data}) => {
            dispatch({type: types.LOADMORE_STATUS_LIST, payload: data});
        })
        .catch(error => {
            dispatch({type: types.LOADMORE_STATUS_LIST_ERROR, payload:  error.response.data});
        })
    }*/
    return dispatch => {
        return dispatch({
            type: "LOADMORE_STATUS_LIST",
            payload: request
        })
    }
}

export function createStatusReport(projectId, params) {
    const url = "projects/"+projectId+"/reports/create";
    const request = client.post(url, params, {
        headers: {
            'Authorization': 'Bearer ' + auth_token,
        }
    });
    /*return (dispatch) => {
        request.then(({data}) => {
            dispatch({type: types.ADD_STATUS, payload: data});
        })
        .catch(error => {
            dispatch({type: types.ADD_STATUS_ERROR, payload:  error.response.data});
        })
    }*/
    return dispatch => {
        return dispatch({
            type: "ADD_STATUS",
            payload: request
        })
    }
}