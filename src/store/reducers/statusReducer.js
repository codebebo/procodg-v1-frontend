import * as types from '../types';
const initialState = { 
    list: null,
    detail: null,
    recent: null
};
export default function statusReducer(state = initialState, action) {  
    switch (action.type) {
        case types.LOADMORE_STATUS_LIST_FULFILLED:
            const payObj = action.payload.data;
            return {
                ...state,
                list: { 
                    error:payObj.error,
                    message:payObj.message,
                    data:state.list.data.concat(payObj.data),
                    paging:payObj.paging
                }
            };
        case types.LOADMORE_STATUS_LIST_REJECTED:
            return {
                ...state, 
                list: {
                    error: action.payload.data.error
                }
            };

        case types.STATUS_LIST_FULFILLED:
            return {
                ...state,
                list: action.payload.data
            }
        case types.STATUS_LIST_REJECTED:
            return {
                ...state, 
                list: action.payload.data
            };
            
        case types.ADD_STATUS_FULFILLED:
            const payAddStatusObj = action.payload.data;
            return {
                ...state,
                list: {
                    error: payAddStatusObj.error,
                    message: payAddStatusObj.message,
                    data: state.list.data.concat(payAddStatusObj.data),
                    paging: state.list.paging
                }
            };
        case types.ADD_STATUS_REJECTED:
            return {
                ...state,
                recent: action.payload.data
            };       
        
        default:
            return state;
    }
}