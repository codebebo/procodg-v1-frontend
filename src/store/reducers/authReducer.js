import * as types from '../types';
const initialState = {  error: null, data: null};
export default function authReducer(state = initialState, action) {  
    switch (action.type) {
    	case types.AUTH_LOGIN:
            return {
                ...state,
                data: action.payload,
                error: null
            };
        case types.AUTH_LOGIN_ERROR:
            return {
                ...state, 
                data: null,
                error: action.payload
            };
        case types.AUTH_LOGOUT:
            return {
                ...state,
                data: action.payload,
                error: null
            };
        case types.AUTH_LOGOUT_ERROR:
            return {
                ...state, 
                data: null,
                error: action.payload
            };
        
        default:
            return state;
    }
}