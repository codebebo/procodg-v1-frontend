import * as types from '../types';
const initialState = {
    list: null,
    detail: null,
    recent: null
};
export default function projectReducer(state = initialState, action) {
    switch (action.type) {

        case types.ADD_PROJECT_FULFILLED:            
            const payAddProObj = action.payload.data;
            return {
                ...state,
                list: {
                    error: payAddProObj.error,
                    message: payAddProObj.message,
                    data: state.list.data.concat(payAddProObj.data),
                    paging: state.list.paging
                }
            };
        case types.ADD_PROJECT_REJECTED:
            return {
                ...state,
                recent: action.payload.response.data
            };

        case types.LOADMORE_PROJECT_LIST_FULFILLED:
            const payObj = action.payload.data;
            return {
                ...state,
                list: {
                    error: payObj.error,
                    message: payObj.message,
                    data: state.list.data.concat(payObj.data),
                    paging: payObj.paging
                }
            };
        case types.LOADMORE_PROJECT_LIST_REJECTED:
            return {
                ...state,
                list: {
                    error: action.payload.data.error
                }
            };

        case types.PROJECT_LIST_FULFILLED:
            return {
                ...state,
                list: action.payload.data
            };
        case types.PROJECT_LIST_REJECTED:
            return {
                ...state,
                list: action.payload.data
            };

        case types.DELETE_PROJECT:
            return {
                ...state,
                recent: action.payload.data
            };

        case types.DELETE_PROJECT_ERROR:
            return {
                ...state,
                recent: action.payload.data
            };

        /*case types.PROJECT_DETAIL:
            return {
                ...state,
                detail: action.payload.data
            };

        case types.PROJECT_DETAIL_ERROR:
            return {
                ...state,
                detail: action.payload.data
            };*/

        case types.UPDATE_PROJECT_FULFILLED:
            const payUpdProObj = action.payload.data;
            const payListProObj = state.list.data;
            let updatedProList = [];
            let proList = payListProObj.filter(function (proList) {
                if(proList.projectID === payUpdProObj.data.projectID) {
                    updatedProList.push(payUpdProObj.data);                    
                } else {
                    updatedProList.push(proList);
                }
            });
            return {
                ...state,
                list: {
                    error: payUpdProObj.error,
                    message: payUpdProObj.message,
                    data: updatedProList,
                    paging: state.list.paging
                }
            };


        case types.UPDATE_PROJECT_REJECTED:
            return {
                ...state,
                recent: action.payload.data
            };

        default:
            return state;
    }
}