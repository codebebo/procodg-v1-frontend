import { combineReducers } from 'redux';
import projectReducer from './reducers/projectReducer';
import statusReducer from './reducers/statusReducer';
import authReducer from './reducers/authReducer';
export default combineReducers({
    projects: projectReducer,
    status: statusReducer,
    auth: authReducer
});