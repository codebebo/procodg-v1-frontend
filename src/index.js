import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import rootReducer from './store/rootReducer';
import 'bootstrap/dist/css/bootstrap.min.css';
import './style.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import promise from "redux-promise-middleware";

const middleware = composeWithDevTools(applyMiddleware(promise, thunk));

const store = createStore(rootReducer,{}, middleware);

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
